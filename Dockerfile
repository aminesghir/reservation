FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY /target/*.jar demo-reservation.jar
CMD ["java", "-jar", "/demo-reservation.jar"]


